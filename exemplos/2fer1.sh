#!/bin/bash

# Pra cada nome de um arquivo, imprimir:
# "Um para NOME, um para mim."

# Quando não houver um nome na lista, imprimir:
# "Um para você, um para mim."

while read nome; do
	echo "Um para ${nome:-você}, um para mim."
done
