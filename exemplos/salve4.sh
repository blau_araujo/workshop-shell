#!/bin/bash

# Imprimir o argumento do echo na saída padrão...
# Cada argumento é uma "palavra" (word).
# Receber argumentos da linha de comandos.
# Receber dados pela entrada padrão.

sauda=(Bem-vindo Salve Olá 'E aí' Fala)

while read nome; do
	si=$(($RANDOM % 5))
	echo "${sauda[si]}, $nome!"
done
