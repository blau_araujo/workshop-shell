#!/bin/bash

# Imprimir o argumento do echo na saída padrão...
# Cada argumento é uma "palavra" (word).
# Receber argumentos da linha de comandos.

vet=("$@")

echo Este vetor tem ${#vet[@]} elementos..
echo Eles são:
echo
printf '• %s\n' "${vet[@]}"

echo O  elemento 3 é: ${vet[3]}
