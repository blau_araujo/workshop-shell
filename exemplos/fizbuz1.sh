#!/bin/bash

# - Se o argumento for divisível por 3, imprimir 'fiz'.
# - Se o argumento for divisível por 5, imprimit 'buz'.
# - Se o argumento for divisível por 3 e 5, imprimir 'fizbuz'.
# - Caso contrário, imprimir o número.
# - Se argumento não for um número, ignorar.


msg='Não é um número!'

for num in "$@"; do
	[[ "$num" =~ ^[1-9][0-9]*$ ]] || { echo $msg; continue; }
	fb=
	(( num % 3 )) || fb=Fiz
	(( num % 5 )) || fb+=Buz
	echo ${fb:-$num}
done

